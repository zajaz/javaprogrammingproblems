import zain.linkedlist.singlelinkedlist.*;

public class Permute{
	public String run(String original){		
		TwoDLL ipList = new TwoDLL();
		SingleLinkedList sl = new SingleLinkedList();
		sl.add(new SLLNode(original.charAt(0)));
		ipList.add(sl);
		for(int i=1;i<original.length();i++){
			ipList = ipList.makeCopies(i+1);
			//ipList.print();
			//System.out.println("\n");	
			char nextChar = original.charAt(i);
			int index = 0;
			for(TwoDLLNode cur=ipList.getHead();cur!=null;cur=cur.next){				
				cur.list.insertAt(index++,new SLLNode(nextChar));
				if(index>cur.list.length())index=0; 
			}
		}
		String permutedString = "";
		int permuteCnt = 0;
		for(TwoDLLNode cur=ipList.getHead();cur!=null;cur=cur.next){
			permutedString += ++permuteCnt +". " +cur.list.toString();
		}
		return permutedString;
	}
	public static void main(String[] args){
		String test = "abc";
		System.out.println("\nAll permutations of the string '" + test + "' are: \n");
		Permute algo = new Permute();
		System.out.println(algo.run(test));
	}
}
