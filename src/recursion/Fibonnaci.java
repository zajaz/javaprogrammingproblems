public class Fibonnaci{
	public static int run(int n){
		if(n==0) return 0;
		if(n==1) return 1;
		return run(n-2)+run(n-1);
	}
	public static int iter(int n){
		int f=0;
		int s=1;
		if(n==0) return 0;
		if(n==1) return 1;
		int result = 0;
		for(int i=2;i<=n;i++){
			result = f+s;
			f=s;
			s=result;
		}
		return result;
	}

	public static void main(String[] args){		
		System.out.println(Fibonnaci.run(6));	
		System.out.println(Fibonnaci.iter(6));
	}
}
