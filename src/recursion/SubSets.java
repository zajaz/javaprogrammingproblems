import java.util.ArrayList;

public class SubSets{
	IndexGenerator indexGenerator;
	String set, _sub_sets="";
	public String getSubSets(String set){
		this.set = set;
		for(int i=0;i<Math.pow(2, set.length());i++){
			ArrayList<Integer> indices = indexGenerator.getIndicesOfSetBits(i);
			_sub_sets += "\n{";			
			for(int j=0;j<indices.size();j++){
				_sub_sets += set.charAt(indices.get(j)) + ", ";				
			}
			_sub_sets += "}";
		}
		return _sub_sets;
	}
	
	public SubSets(IndexGenerator indexGenerator){
		this.indexGenerator = indexGenerator;
	}
	
	public static void main(String[] args){
		SubSets algo = new SubSets(new IndexGenerator());		
		System.out.println(algo.getSubSets("abc"));
	}
}
