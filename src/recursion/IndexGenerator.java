import java.util.ArrayList;

public class IndexGenerator{
	public ArrayList<Integer> getIndicesOfSetBits(int num){
		ArrayList<Integer> setBits = new ArrayList<>();
		for(int count=0,tmp=num;tmp >0; tmp=tmp>>>1){
			if((tmp &1) > 0) setBits.add(count);
			count++;
		}
		return setBits;
	}
}
