public class FloodFill{
	public void run(int[][] screen, int oc, int nc, int x, int y){
		if(x<0 || x>=screen[0].length || y<0 || y>=screen.length) return;
		if(screen[y][x] == oc) {
			screen[y][x] = nc;
			run(screen,oc, nc, x-1,y);
			run(screen,oc, nc, x+1,y);
			run(screen,oc, nc, x,y-1);
			run(screen,oc, nc, x,y+1);
		}
	}
	public static void traverseAnd(int[][] screen, String action){
		for(int i=0;i<screen[0].length;i++){
			for(int j=0;j<screen.length;j++){
				if(action == "print") System.out.print(screen[i][j] + " ");
				if(action == "init") {
					if(i==0 || i==screen.length-1 ||j==0 || j==screen[0].length-1) screen[i][j] = 2;
					else screen[i][j] = 1;
				}
			}
			if(action == "print") System.out.println("");
		}
	}
	public static void main(String[] args){
		FloodFill algo = new FloodFill();
		int[][] screen = new int[4][4];
		traverseAnd(screen, "init");
		traverseAnd(screen, "print");
		System.out.println("After FloodFill");
		algo.run(screen, 1,0,2,2);
		traverseAnd(screen, "print");		
	}
}
