/*
	Self notes: always draw a matrix first.
	1: Three anti-clock swaps => clock wise rotation. 
        	 Visualize as picking one layer up and sliding others.
	2: Number of layers is n over 2.
	3: Each layer of n points, only needs n-1 points to rotate it =>
			moving from vertical to horizontal index needs to be adjusted by one.
	
	If space is allowed, you can simply copy the row of one into to other as columns.
*/
public class ImageRotation{
	public void layerRotate(int[][] A, int layer, int max){
		for(int i=layer;i<max;i++){
			int temp = A[max-i+layer][layer];
			A[max-i+layer][layer] = A[layer][i];
			A[layer][i]=temp;
		}
		for(int i=layer;i<max;i++){
			int temp = A[max-i+layer][layer];
			A[max-i+layer][layer] = A[max][max-i+layer];
			A[max][max-i+layer]=temp;
		}
		//max=0;
		for(int i=layer;i<max;i++){
			int temp = A[max][max-i+layer];
			A[max][max-i+layer] = A[i][max];
			A[i][max]=temp;
		}
	}

	public void rotateRight(int[][] A){
		for(int layer=0;layer<(A.length)/2;layer++) layerRotate(A, layer, A.length-1-layer);
		
	}
	
	public void traverse(int[][] A, String action){
		int count=0;
		for(int i=0;i<A.length;i++){
			for(int j=0;j<A.length;j++){
				if(action == "init") A[i][j] = ++count;
				else if(action =="print") System.out.print(A[i][j] + "	 "); 
			}
			if(action == "print")System.out.println("");
		}
	}

	public static void main(String[] args){
		int[][] A = new int[7][7];
		ImageRotation imageRotate = new ImageRotation();
		imageRotate.traverse(A,"init");
		System.out.println("Original");
		imageRotate.traverse(A,"print");
		System.out.println("New");
		imageRotate.rotateRight(A);
		imageRotate.traverse(A,"print");
	}
}
