import zain.Matrix;
public class SpiralPrint{
	public static void printSpiral(int[][] A){
		int xmin=0,xmax=A[0].length-1,ymin=0,ymax=A.length-1;
		while((xmin<A[0].length/2) && (ymin<A.length/2)){
			for(int i=xmin;i<xmax;i++) System.out.println(A[xmin][i]);
			for(int j=ymin;j<ymax;j++) System.out.println(A[j][xmax]);
			for(int i=xmax;i>xmin;i--) System.out.println(A[ymax][i]);
			for(int j=ymax;j>ymin;j--) System.out.println(A[j][xmin]);
			xmin+=1; ymin+=1;
			xmax-=1; ymax-=1;
		}
		if(xmin==xmax && ymin==ymax) System.out.println(A[xmin][xmax]);
		else if(xmin==xmax && ymin<ymax){
			for(int i=ymin;i<=ymax;i++) System.out.println(A[i][xmin]);
		}
		else if(ymin==ymax && xmin<xmax){
			for(int i=xmin;i<=xmax;i++) System.out.println(A[ymin][i]);
		}
	}
	
	public static void main(String[] args){
		//int[][] A = new int[4][4];
		int[][] A = new int[5][5];
		//int[][] A = new int[5][3];
		//int[][] A = new int[3][5];
		Matrix.init(A);
		System.out.println("Original");
		Matrix.print(A);
		printSpiral(A);
	}
}
