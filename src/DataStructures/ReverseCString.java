public class ReverseCString{
	public static void reverse(char[] input){
		for(int i=0; i<input.length/2;i++){
			char temp = input[i];
			input[i] = input[input.length-(i+1)];
			input[input.length-(i+1)]=temp;//+1 to adjust length for 0 indexing
		}
	}
	
	public static void print(char[] str){
		for(int i=0;i<str.length;i++) System.out.println(str[i]);
	}	
	
	public static void main(String[] args){
		char[] test_str = {'1','2','3','l','l','e','h'};
		//char[] test_str = {'1','2','3','4','l','l','e','h'};
		System.out.println("Original");
		print(test_str);
		System.out.println("reversed");
		reverse(test_str);
		print(test_str);		 
	}
}
