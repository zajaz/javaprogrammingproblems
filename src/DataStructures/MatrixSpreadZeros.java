import java.util.ArrayList;
import zain.Matrix;
import zain.Point;

public class MatrixSpreadZeros{

	public static void spread(int[][] A){
		ArrayList<Point> zerosAt = new ArrayList<>();
		int rows=A[0].length, columns=A.length;
		for(int i=0;i<rows;i++){
			for(int j=0;j<columns;j++){
				if(A[i][j] == 0) zerosAt.add(new Point(i,j));
			}
		}
		
		for(int i=0; i<zerosAt.size(); i++){
			Point p = zerosAt.get(i);
			for(int j=0;j<columns;j++) A[p.x][j] = 0;
			for(int j=0;j<rows;j++) A[j][p.y] = 0;
		}
	}
	
	public static void main(String[] args){
		int[][] A = new int[6][6];
		Matrix.init(A);
		A[0][0]=0;
		A[5][5]=0;
		System.out.println("Before:");
		Matrix.print(A);
		spread(A);
		System.out.println("\nAfter:");		
		Matrix.print(A);
	}
}
