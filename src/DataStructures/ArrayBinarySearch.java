public class ArrayBinarySearch{
	int result,index,val;
	public int search(int[] a, int val){
		this.val=val;
		bSearch(a,0,a.length-1, val);
		return result;
	}
	private void bSearch(int[] a,int left, int right, int val){
		if(left>right){
			result=-1;
			return;
		}
		int mid = (left+right) >> 1;
		if(a[mid]==val){
			index=mid;
			result=1;
			return;
		}
		else if(a[mid] > val) bSearch(a,left,mid-1,val);
		else bSearch(a,mid+1,right,val);
	} 
	public void printResult(){
		if(result==-1)  System.out.println("Didn't find "+val);
		else 			System.out.println("Found " + val+" at "+index);
	}
	public static void main(String[] args){
		int[] a = {2,4,6,8,10};
		ArrayBinarySearch algo = new ArrayBinarySearch();
		algo.search(a,7);
		algo.printResult();
	}
}
