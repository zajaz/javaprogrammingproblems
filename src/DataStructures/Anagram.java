
import java.util.HashMap;
public class Anagram{
	public static boolean AnagramChecker(char[] s1, char[] s2){
		if(s1.length != s2.length) return false;
		HashMap<Character, Integer> score_table = new HashMap<>();
		for(int i=0;i<s1.length;i++){
			if(score_table.containsKey(s1[i])) score_table.put(s1[i], score_table.get(s1[i])+1);
			else score_table.put(s1[i], 1);
		}

		for(int i=0;i<s1.length;i++){
			if(!(score_table.containsKey(s2[i]))) return false;
			else score_table.put(s2[i], score_table.get(s2[i])-1);
		}
		
		for(Integer value:score_table.values()) if(value > 0) return false;
		return true;
	}
	
	public static void main(String[] args){
		char[] s1 = {'1','2','3','4','1','1','1','1'};
		char[]s2 = {'a','b'};
		System.out.println("Testing Anargrams returns: ");
		System.out.println(AnagramChecker(s1,s1));
		System.out.println("Testing NON Anargrams returns: "); 
		System.out.println(AnagramChecker(s1,s2));
	}
}
