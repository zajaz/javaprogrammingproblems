import java.util.HashSet;
/*
for each doesnt work with String.
String doesn't work like an array, name[i] not supported.
*/
public class HasUniqueChars{
	HashSet<Character> wasSeen = new HashSet<>();

	public boolean test(String input){
		for(int i =0; i<input.length(); i++){
			if(wasSeen.contains(input.charAt(i))) return false;
			wasSeen.add(input.charAt(i));
		}
		wasSeen.clear();
		return true;
	}
	
	public boolean testInPlace(String input){
		for(int i=0;i<input.length();i++){
			for(int j=i+1;j<input.length(); j++){
				//System.out.println("testing: "+input.charAt(i)+" == "+input.charAt(j));
				if(input.charAt(i) == input.charAt(j)) return false;
			}
		}
		return true;
	}
}
