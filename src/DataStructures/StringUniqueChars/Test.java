public class Test{
	public static void main(String[] args){
		String nr = "abcde";
		String rep = "abcda";
		
		HasUniqueChars hasUniqueChars = new HasUniqueChars();
		if (hasUniqueChars.test(nr)) System.out.println("Unique:"+nr);
		else System.out.println("Not unique: "+nr);
		
		if (hasUniqueChars.test(nr)) System.out.println("Unique:"+nr);
		else System.out.println("Not unique: "+nr);

		System.out.println("\nTesting inplace");
		if (hasUniqueChars.testInPlace(nr)) System.out.println("Unique:"+nr);
		else System.out.println("Not unique: "+nr);

		if (hasUniqueChars.testInPlace(rep)) System.out.println("Unique:"+rep);
		else System.out.println("Not unique: "+rep);

	}
}
