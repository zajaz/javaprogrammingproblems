public class SpaceReplace{
	public static void replace(char[] str, int len){
		int space_count = 0;
		for(int i=0;i<len;i++) if(str[i] == ' ') space_count++;

		int shift_size = space_count*2;
		int current = str.length-1;
		System.out.println("len "+len+" shift size: "+shift_size+" current: "+current);
		while(shift_size > 1){
			if(str[current-shift_size] == ' '){
				shift_size -= 2;
				str[current] = '0';current--;
				str[current] = '2';current--;
				str[current] = '%';current--;
			}
			else{
				str[current] = str[current - shift_size];
				current--;
			}
		}
	}
	
	public static void print(char[] str){
		for(int i=0;i<str.length;i++) System.out.println(str[i]);
	}

	public static void main(String[] args){
		char[] test = {'a',' ',' ','b','1','1','1','1'};
		System.out.println("Original");
		print(test);
		System.out.println("Modified");
		replace(test,4);
		print(test);
	}
}
