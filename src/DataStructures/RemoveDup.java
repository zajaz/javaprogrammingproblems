class RemoveDup{
	public static void print(char[] str){
		for(int i=0;i<str.length;i++) System.out.println(str[i]);
	}
	public static void main(String[] args){
		char[] test = {'1','1','1','1','1','1','1'};
		//char[] test = {'1','2','3','4'};
		System.out.println("Original: ");
		print(test);
		char[] trimmed = trim(test);
		System.out.println("Trimmed: ");
		print(trimmed);		
	}

	public static void remove_at(char[] str, int index, int new_len){
		for(int i=index; i<new_len-1;i++) str[i] = str[i+1];
	}
	//End index of array is len-1
	public static char[] trim(char[] str){
		int new_length = str.length;
		for(int i=0;i<new_length-1;i++){
			for(int j=new_length-1;j>i;j--){
				if(str[i]==str[j]){
					remove_at(str, j, new_length);
					new_length--;
				}
			}
		}
		char[] trimmed = new char[new_length];
		for(int i=0;i<new_length;i++) trimmed[i] = str[i];
		return trimmed;
	}

}
