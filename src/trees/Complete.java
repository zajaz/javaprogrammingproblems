import zain.BstNode;
import zain.Bst;
public class Complete{
	public boolean run(Bst tree){
		int numNodes = findNumOfNodes(tree.get_root());
		return isComp(tree.get_root(),0,numNodes);		
	}

	public boolean isFull(Bst tree){
		return isFull(tree.get_root());
	}
	private int findNumOfNodes(BstNode root){
		if(root==null)return 0;
		return 1 + findNumOfNodes(root.left) + findNumOfNodes(root.right);
	}
	
	private boolean isFull(BstNode root){
		if(root==null) return true;
		if(root.left==null && root.right==null) return true;
		if(root.left!=null && root.right!=null) return isFull(root.left) && isFull(root.right);
		return false;
	}
	
	private boolean isComp(BstNode root, int index, int numNodes){
		if(root == null) return true;
		if(index >= numNodes) return false;
		return isComp(root.left, index*2+1, numNodes) && isComp(root.right,index*2+2,numNodes);
	}

	public static void main(String[] args){
		Bst Ltree = new Bst();
		Ltree.insert(new BstNode(4));
		Ltree.insert(new BstNode(2));
		Ltree.insert(new BstNode(6));
		Ltree.insert(new BstNode(1));
		Ltree.insert(new BstNode(3));
		Ltree.insert(new BstNode(5));
		Ltree.insert(new BstNode(7));
		Ltree.insert(new BstNode(-1));
		//Ltree.insert(new BstNode(11));
		Complete algo = new Complete();
		if(algo.run(Ltree))System.out.println("Tree is complete.");
		else 		System.out.println("Incomplete Tree.");
		if(algo.isFull(Ltree))System.out.println("Tree is Full.");
		else 		System.out.println("Tree is not full.");
	}
}
