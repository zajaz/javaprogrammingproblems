import zain.Bst;
import zain.BstNode;
import zain.LinkedListMaker;
import java.util.LinkedList;

public class DepthLists{

	LinkedListMaker listMaker;
	
	public DepthLists(LinkedListMaker listMaker){
		this.listMaker = listMaker;
	}	
	
	public void run(BstNode root, int depth){
		if(root==null) return;
		listMaker.add(depth, root);
		depth++;
		run(root.left, depth);
		run(root.right, depth);
	}

	public int numberOfLists(){return listMaker.size();}	
	
	public LinkedList<BstNode> get_list(int height){return listMaker.get(height);}	

	public static void main(String[] clArgs){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(7));

		DepthLists algo = new DepthLists(new LinkedListMaker());
		algo.run(tree.get_root(), 0);
		
		for(int i=0; i<algo.numberOfLists();i++){
			LinkedList<BstNode> list = algo.get_list(i);
			System.out.println("List at height "+i+" has ");
			for(BstNode node: list) System.out.println(node.data);
			System.out.println("");
		}
	}
}
