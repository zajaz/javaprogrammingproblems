import zain.Bst;
import zain.BstNode;

public class PrintStars{
	public int run(BstNode root, int depth, boolean is_left){
		if(root==null) return 0;

		depth++;

		int left_height = 1 + run(root.left, depth, true);

		int right_height = 1 + run(root.right, depth, false);

		if(depth == left_height && left_height == right_height)
			System.out.println("Star node value: "+root.data);

		if (is_left) return left_height;
		else return right_height; 
	}
	
	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(7));
		//tree.print();
		PrintStars algo = new PrintStars();
		algo.run(tree.get_root(), 0, false);
	}
}
