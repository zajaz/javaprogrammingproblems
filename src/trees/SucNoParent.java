import zain.Bst;
import zain.BstNode;

public class SucNoParent{
	public BstNode run(BstNode root, BstNode item){
		if(item.right!=null){
			BstNode cur = item.right;
			while(cur.left!=null) cur=cur.left;
			return cur;
		}
		
		BstNode suc=null, smallestRP=null;
		while(root!=item){
			if(root.data > item.data){
				smallestRP = root;
				root = root.left;
			}
			else root = root.right;
			suc = smallestRP;
		}
		return suc;
	}

	public static void main(String[] clArgs){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(7));
		BstNode root = tree.get_root();
		SucNoParent algo = new SucNoParent();
		BstNode cur = tree.get_root();
		//cur = cur.left.right;
		cur = cur.right.right;
		//cur = cur.left.left;

		BstNode suc_p = algo.run(root, cur);
		if(suc_p==null) System.out.println("Successor of " + cur.data + " doesn't exist.");
		else            System.out.println("Successor of " + cur.data + " is "+ suc_p.data);	
	}
}
