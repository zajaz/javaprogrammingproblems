import zain.Bst;
import zain.BstNode;

public class SumLeftLeaves{
	int sum = 0;
	public int run(Bst tree){
		findSum(tree.get_root(), false);
		int rv = sum;
		sum = 0;
		return rv;
	}
	private void findSum(BstNode root, boolean isLeft){
		if(root==null)return;
		if(isLeft && root.left==null && root.right==null) sum+=root.data;
		findSum(root.left,true);
		findSum(root.right,false);
	}
	
	public static void main(String[] args){
		Bst Ltree = new Bst();
		Ltree.insert(new BstNode(4));
		Ltree.insert(new BstNode(2));
		Ltree.insert(new BstNode(6));
		Ltree.insert(new BstNode(1));
		Ltree.insert(new BstNode(3));
		Ltree.insert(new BstNode(5));
		Ltree.insert(new BstNode(7));
		Ltree.insert(new BstNode(37));
		Ltree.insert(new BstNode(11));
		SumLeftLeaves algo = new SumLeftLeaves();
		System.out.println("Sum of left leaves is: "+algo.run(Ltree));
	}
}
