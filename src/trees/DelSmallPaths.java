import zain.Bst;
import zain.BstNode;

public class DelSmallPaths{
	public void run(Bst tree, int min_path){
		del(tree.get_root(),0,min_path);
	}
	
	private BstNode del(BstNode root, int depth, int min_path){
			if(root==null)return null;
			depth++;
			root.left = del(root.left,depth, min_path);
			root.right = del(root.right,depth, min_path);
			if(root.left==null && root.right==null && depth<min_path) return null;
			return root; 
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(7));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(8));
		tree.print();
		DelSmallPaths algo = new DelSmallPaths();
		algo.run(tree, 4);
		System.out.println("\nAfter deleting paths below 5:");
		tree.print();		
	}
}
