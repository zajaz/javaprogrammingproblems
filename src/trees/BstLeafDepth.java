import zain.Bst;
import zain.BstNode;

public class BstLeafDepth{
	public int max_depth = 0;
	// boolean is_balanced = false;can't be done with preorder var height
	// as you will need the height info to travel up.
	// #todo: use a post order or inorder var to send the height info up to root.

	public void run(BstNode root, int height){
		if(root == null) return;		
		
		if(root.left == null && root.right == null){
			System.out.println(height);
			if(height > max_depth) max_depth = height;
		}
		height++;
		run(root.left, height);
		run(root.right, height);		
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(7));
		tree.print();
		System.out.println("Depth of leaves.");
		BstLeafDepth algo = new BstLeafDepth();
		algo.run(tree.get_root(), 0);
		System.out.println("Maximum depth is:" + algo.max_depth);		
	}
}
