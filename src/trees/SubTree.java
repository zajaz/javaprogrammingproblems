//Note: Uses parallel traversal of trees.

import zain.BstNode;
import zain.Bst;

public class SubTree{
	boolean is_subtree, found;
	BstNode small_root;
	
	public boolean isSubTree(BstNode Lroot, BstNode Sroot){
		is_subtree = true;
		findSmall(Lroot,Sroot);
		if(small_root ==null) System.out.println("Unable to find the small root.");
		testIsSubTree(small_root, Sroot);
		return is_subtree;		
	}

	private void findSmall(BstNode Lroot, BstNode Sroot){
		if(Lroot==null) return;
		if(!found && Lroot.data==Sroot.data){
			found = true;
			small_root = Lroot;
		}
		findSmall(Lroot.left, Sroot);
		findSmall(Lroot.right, Sroot);
	}
	
	private void testIsSubTree(BstNode Lroot, BstNode Sroot){
		if(Sroot==null) return;
		else if(Lroot==null){
			is_subtree = false;
			return;
		}
		if(Lroot.data != Sroot.data) is_subtree = false;
		testIsSubTree(Lroot.left, Sroot.left);
		testIsSubTree(Lroot.right, Sroot.right);
	}
	
	public static void main(String[] args){
		Bst Ltree = new Bst();
		Ltree.insert(new BstNode(4));
		Ltree.insert(new BstNode(2));
		Ltree.insert(new BstNode(6));
		//Ltree.insert(new BstNode(1));
		//Ltree.insert(new BstNode(3));
		//Ltree.insert(new BstNode(5));
		Ltree.insert(new BstNode(7));
		
		Bst Stree = new Bst();
		Stree.insert(new BstNode(4));
		Stree.insert(new BstNode(2));
		Stree.insert(new BstNode(6));
		Stree.insert(new BstNode(1));
		
		
		SubTree algo = new SubTree();
		if(algo.isSubTree(Ltree.get_root(), Stree.get_root())) System.out.println("It is a subtree.");
		else System.out.println("Its not a subtree.");
	}	

}
