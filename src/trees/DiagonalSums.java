import java.util.ArrayList;
import zain.*;

public class DiagonalSums{
	public ArrayList<Integer> run(Bst tree){
		ArrayList<Integer> sums = new ArrayList<>();
		findSums(tree.get_root(), 0, sums);
		return sums;
	}
	
	private void findSums(BstNode root, int diagonalNo, ArrayList<Integer> sums){
		if(root==null) return;
		if(sums.size()-1 < diagonalNo) sums.add(diagonalNo,0);
		sums.set(diagonalNo,(sums.get(diagonalNo)+root.data));
		findSums(root.left,diagonalNo+1,sums);
		findSums(root.right,diagonalNo,sums);
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(7));
		DiagonalSums algo = new DiagonalSums();
		ArrayList<Integer> sums = algo.run(tree);
		for(int i=0;i<sums.size();i++){
			System.out.println("Sum at Diagonal "+i+" is "+sums.get(i));
		}
	}
}
