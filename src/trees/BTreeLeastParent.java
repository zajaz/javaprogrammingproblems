import zain.Bst;
import zain.BstNode;

public class BTreeLeastParent{
	
	boolean found=false;
	BstNode lP=null, first=null, second=null;

	public BstNode findLP(BstNode root, BstNode first, BstNode second){
		if(first.data == second.data) return first;
		this.first = first;
		this.second = second;
		find(root);
		return lP;
	}
	
	private boolean is_child(BstNode root){
		return root.data==first.data || root.data==second.data;
	}	

	private boolean find(BstNode root){
		if(root == null) return false;
		boolean left = find(root.left);
		boolean right= find(root.right);
		if(!found){
			if(left && right){
				found = true;
				lP = root;
			}
			else if(is_child(root) &&(left || right)){
				found = true;
				lP = root;
			}
		}
		if(left || right) return true;		
		if(is_child(root))return true;
		return false;
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(7));

		BTreeLeastParent algo = new BTreeLeastParent();
		BstNode root = tree.get_root(), first, second;
		first = root.left.right;
		second = root.right;
		BstNode lP = algo.findLP(root,first,second);		
		if(lP!=null) System.out.println("The least parent of "+first.data+" and "+second.data+" is "+lP.data);
		else System.out.println("The least parent of "+first.data+" and "+second.data+" doesn't exist");
	}
}
