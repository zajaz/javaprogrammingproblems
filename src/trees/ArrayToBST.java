import zain.BstNode;

public class ArrayToBST{
	int[] Ary;
	public BstNode create(int[] ary){
		Ary = ary;
		return insert(0,Ary.length-1);
	}

	private BstNode insert(int s, int e){
		if(s>e) return null;
		int mid = (s+e)/2;
		BstNode root = new BstNode(Ary[mid]);		
		root.left = insert(s,mid-1);
		root.right = insert(mid+1,e);
		return root;
	}
	
	public static void print(BstNode root){
		if(root == null) return;
		print(root.left);
		System.out.println(root.data);
		print(root.right);
	}
	
	public static void main(String[] args){
		int[] A = {1,2,3,4,5};
		// int[] A = {1,2,3,4,5,6};
		ArrayToBST algo = new ArrayToBST();
		BstNode cur = algo.create(A);
		print(cur);
	}
}
