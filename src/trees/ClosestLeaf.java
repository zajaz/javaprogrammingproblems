import zain.*;
import java.util.ArrayList;
public class ClosestLeaf{
	int result,val;

	private int findMinChildLeaf(BstNode root){
		if(root==null) return Integer.MAX_VALUE;
		if(root.left==null && root.right==null) return 0;
		return 1+ Math.min(findMinChildLeaf(root.left),findMinChildLeaf(root.right));
	}
	
	private void findMinLeaf(BstNode root, ArrayList<BstNode> list, int depth){
		if(root==null) return;
		if(root.data == val){
			int downDepth = findMinChildLeaf(root);
			result = downDepth;
			for(int i=list.size()-1;i>=0;i--){
				result = Math.min(downDepth, list.size()-i+findMinChildLeaf(list.get(i)));
			}
		}
		else{
			list.add(depth, root);
			depth++;
			findMinLeaf(root.left, list, depth);
			findMinLeaf(root.right, list, depth);
		}
	}
	
	public int run(Bst tree, int val){
		this.val=val;
		findMinLeaf(tree.get_root(), new ArrayList<BstNode>(), 0);
		return result;
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(7));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(9));
		tree.insert(new BstNode(11));
		ClosestLeaf algo = new ClosestLeaf();
		System.out.println("\nDistance to closest leaf: "+ algo.run(tree, 7));
	}
}
