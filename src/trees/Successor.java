import zain.BstWithParent;
import zain.BstNodeP;

public class Successor{
	private BstNodeP suc_p;
	
	private void findSuc(BstNodeP root){
		suc_p = null;
		if(root == null) return;
		if(root.right != null){
			BstNodeP cur=root.right;
			while(cur.left!=null) cur = cur.left;
			suc_p = cur;
			return;
		}
		if(root.parent!=null) {
			while(root.parent != null){
				if(root.parent.left == root) {
					suc_p = root.parent;
					return;
				}
				root = root.parent;
			}
		}
		
	}
	
	public BstNodeP find_suc(BstNodeP item){
		findSuc(item);
		return suc_p;
	}
	
	public static void main(String[] clArgs){
		BstWithParent tree = new BstWithParent();
		tree.insert(new BstNodeP(4));
		tree.insert(new BstNodeP(2));
		tree.insert(new BstNodeP(6));
		tree.insert(new BstNodeP(1));
		tree.insert(new BstNodeP(3));
		tree.insert(new BstNodeP(5));
		tree.insert(new BstNodeP(7));
		BstNodeP root = tree.getRoot();
		tree.print();

		Successor algo = new Successor();

		BstNodeP cur = tree.getRoot();
		//BstNodeP cur = tree.getRoot().left.right;
		//BstNodeP cur = tree.getRoot().right.right;
		//BstNodeP cur = tree.getRoot().left.left;

		BstNodeP suc_p = algo.find_suc(cur);
		if(suc_p==null) System.out.println("Successor of " + cur.data + " doesn't exist.");
		else            System.out.println("Successor of " + cur.data + " is "+ suc_p.data);	
	}
}
