public class SegmentTree{
	private int[] tree;
	private int[] ary;

	public void createTree(int[] ary){
		this.ary=ary;
		tree = new int[7];//n log n
		createHelper(0,ary.length-1, 0);
	}

	private void createHelper(int s, int e, int index){
		if(s==e){
			tree[index]=ary[s];
			return;
		}
		int left = index*2+1;
		int right = index*2+2;
		int mid = s+((e-s)/2);
		createHelper(s, mid, left);
		createHelper(mid+1,e,right);
		tree[index] = tree[left] + tree[right];
	}
	public int getSum(int qs, int qe){
		return getSumHelper(0,ary.length-1,qs,qe,0);
	}
	private int getSumHelper(int s, int e, int qs, int qe, int index){
		if(qs<=s && e<=qe) return tree[index];
		if(e<qs || s>qe) return 0;
		int mid = s+((e-s)/2);
		return getSumHelper(s,mid,qs,qe,2*index+1) + getSumHelper(mid+1,e,qs,qe,2*index+2);
	}
	public void print(){
		System.out.println();
		for(int i=0;i<tree.length;i++)	System.out.print(tree[i]+" ");
		System.out.println();
	}
	
	public void update(int i, int val){
		int dif = val - ary[i];//reverse diff cause error
		ary[i] = val;
		updateTree(0,ary.length-1,i,dif,0);
	}

	private void updateTree(int s, int e, int i, int diff, int index){
		if(i<s||i>e) return;
		tree[index] += diff;
		if(s!=e){
			int mid = s + (e-s)/2;
			updateTree(s,mid,i,diff,index*2+1);
			updateTree(mid+1,e,i,diff,2*index+2);
		}
	}

	public static void main(String[] args){
		int[] ary = {1,2,3,4};
		SegmentTree algo = new SegmentTree();
		algo.createTree(ary);
		algo.print();
		System.out.println("Sum of (0,3): "+algo.getSum(0,3));
		System.out.println("Sum of (0,2): "+algo.getSum(0,2));
		System.out.println("Sum of (1,2): "+algo.getSum(1,2));
		algo.update(0, 2);
		algo.print();
		System.out.println("Sum of (0,3): "+algo.getSum(0,3));
		System.out.println("Sum of (0,2): "+algo.getSum(0,2));
		System.out.println("Sum of (1,2): "+algo.getSum(1,2));		
	}
}
