import zain.BstNode;
import zain.Bst;

public class PrintAtDist{
	boolean found;
	int distance,val;

	public void print(Bst tree, int val, int distance){
		this.val = val;this.distance=distance;
		print(tree.get_root(),tree.get_root(),0,0);
	}
	
	private void print(BstNode root, BstNode upper,int r_dist, int f_dist){
		if(root==null || upper==null) {if(root==null) found=false;return;}
		
		if(root.data == val && !found){
			found=true;
			//System.out.println("Upper: "+upper.data);
		}
	
		if(r_dist > (distance-1) && !found) {
			//print(root, upper.left,r_dist, f_dist);
			//print(root, upper.right,r_dist, f_dist);
		}

		if(f_dist == distance) System.out.println(root.data);

		if(found){
			print(root.left,upper,r_dist+1,f_dist+1);
			print(root.right,upper,r_dist+1,f_dist+1);
		}
		else{
			print(root.left,upper,r_dist+1,0);
			print(root.right,upper,r_dist+1,0);
		}
	}
	
	public static void main(String[] args){
		Bst Ltree = new Bst();
		Ltree.insert(new BstNode(4));
		Ltree.insert(new BstNode(2));
		Ltree.insert(new BstNode(6));
		Ltree.insert(new BstNode(1));
		Ltree.insert(new BstNode(3));
		Ltree.insert(new BstNode(5));
		Ltree.insert(new BstNode(7));
		PrintAtDist algo = new PrintAtDist();
		algo.print(Ltree, 6,1);
	}
}
