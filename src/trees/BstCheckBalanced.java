import zain.BstNode;
import zain.Bst;

public class BstCheckBalanced{
	public boolean check(BstNode root){
		int min_height, max_height;
		min_height = min_h(root);
		max_height = max_h(root);
		return max_height - min_height <= 1;
	}

	private int min_h(BstNode root){
		if(root == null) return 0;
		else return 1 + Math.min(min_h(root.left), min_h(root.right));
	}

	private int max_h(BstNode root){
		if(root == null) return 0;
		else return 1 + Math.max(max_h(root.left), max_h(root.right));
	}
	
	public static void main(String[] args){
		Bst tree = new Bst();
		BstCheckBalanced checker = new BstCheckBalanced();
		//for(int i=0;i<10;i++) tree.insert(new BstNode(i));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(1));
		if(checker.check(tree.get_root())) System.out.println("Is balanced");
		else							  System.out.println("Not balanced");
	}
}
