import zain.Bst;
import zain.BstNode;

public class BstDescPrint{
	public static void run(BstNode root){
		if(root == null) return;
		run(root.right);
		System.out.println(root.data);
		run(root.left);
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(3));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(2));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(1));
		run(tree.get_root());		
	}
}
