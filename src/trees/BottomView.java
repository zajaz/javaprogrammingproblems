import zain.*;
import java.util.HashMap;
//import java.util.Map;

public class BottomView{
	class Item{
		public int key;
		public int height;
		public Item(int k , int h){key=k;height=h;}
	}
	HashMap<Integer, Item> list;

	public void run(Bst tree){
		list  = new HashMap<>();
		createView(tree.get_root(), 0 , 0);
		System.out.println("BottomView: ");
		//for(Map.Entry<Integer,Item> entry:list.entrySet()){
		//	System.out.println(entry.getKey()+": "+entry.getValue().key);
		for(Integer k:list.keySet()){
			System.out.println(k+": "+list.get(k).key);
				
		}
	}

	private void createView(BstNode root, int x, int y){
		if(root==null) return;
		if(!list.containsKey(x) || list.get(x).height < y) 
			list.put(x, new Item(root.data, y));
		createView(root.left, x-1, y+1);
		createView(root.right, x+1, y+1);
	}

	public static void main(String[] args){
		Bst tree = new Bst();
		tree.insert(new BstNode(4));
		tree.insert(new BstNode(1));
		tree.insert(new BstNode(7));
		tree.insert(new BstNode(6));
		tree.insert(new BstNode(5));
		tree.insert(new BstNode(9));
		tree.insert(new BstNode(11));
		BottomView algo = new BottomView();
		algo.run(tree);
	}
}
