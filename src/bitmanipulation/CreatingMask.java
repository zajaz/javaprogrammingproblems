public class CreatingMask{
	public void test(){
		int num = Integer.parseInt("10101010",2);
		int mask = ((1<<6)-1)^((1<<2)-1);
		System.out.println("num              :"+Integer.toBinaryString(num));
		System.out.println("Mask             :"+Integer.toBinaryString(mask));
		System.out.println("Subtituted result:"+Integer.toBinaryString(num|mask));
	}
	
	public static void main(String[] args){
		CreatingMask algo = new CreatingMask();
		algo.test();
	}
}
