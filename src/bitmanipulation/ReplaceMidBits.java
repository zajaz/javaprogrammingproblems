public class ReplaceMidBits{
	public void run(int N, int M, int end, int start){
		System.out.println("original N: " +Integer.toBinaryString(N));
		System.out.println("original M: " +Integer.toBinaryString(M));
		int mask = ((1<<end+1)-1) ^ ((1<<start)-1); //creates 1 upto i and j and ^ keeps them only in the center.
		int data = M & mask;
		//clearing;
		N = N & ~mask;
		//set
		N = N|data;
		System.out.println("New      N: " +Integer.toBinaryString(N));		
	}

	public static void main(String[] args){
		int N = Integer.parseInt("10000111",2);
		int M = Integer.parseInt("01111000",2);
		ReplaceMidBits algo = new ReplaceMidBits();
		algo.run(N,M,6,3);
	}
}
