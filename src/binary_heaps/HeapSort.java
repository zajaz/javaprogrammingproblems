public class HeapSort{
	public void desc(int[] A){
		build(A);
		for(int i=A.length-1;i>=0;i--){			
			bubble_down(A, 0, i);
			int tmp = A[0];
			A[0] = A[i];
			A[i] = tmp;
		}
	}
	private void build_a(int[] A){
		for(int i=(int)Math.floor(A.length-1/2);i>=0;i--)
			bubble_down_a(A,i,A.length-1);
	}
	
	public void asc(int[] A){
		build_a(A);
		for(int i=A.length-1;i>=0;i--){
			int tmp = A[0];
			A[0] = A[i];
			A[i] = tmp;
			bubble_down_a(A, 0, i-1);
		}
	}
	
	private void bubble_down_a(int[] A, int root, int len){
		int max = root;
		int l = root*2+1;
		int r = root*2+2;
		if(l <= len && A[l]>A[max]) max = l;
		if(r <= len && A[r]>A[max]) max = r;
		if(max != root){
			int tmp = A[root];
			A[root] = A[max];
			A[max] = tmp;
			bubble_down(A, max,len);
		}		
	}
	
	private void build(int[] A){
		for(int i=(int)Math.floor(A.length-1/2);i>=0;i--)
			bubble_down(A,i,A.length-1);
	} 
	
	private void bubble_down(int[] A, int root, int len){
		int min = root;
		int l = root*2+1;
		int r = root*2+2;
		if(l <= len && A[l]<A[min]) min = l;
		if(r <= len && A[r]<A[min]) min = r;
		if(min != root){
			int tmp = A[root];
			A[root] = A[min];
			A[min] = tmp;
			bubble_down(A, min,len);
		}		
	} 

	public static void main(String[] args){
		int A[] = {1,2,3,4,5,6,7,8,9};
		System.out.print("Original: \n");
		for(int i=0;i<A.length;i++) System.out.print(A[i] +", ");
		System.out.print("\nSorted: \n");
		HeapSort algo = new HeapSort();
		algo.desc(A);
		for(int i=0;i<A.length;i++) System.out.print(A[i] +", ");
		System.out.print("\n");
		algo.asc(A);
		for(int i=0;i<A.length;i++) System.out.print(A[i] +", ");
		System.out.print("\n");	
	}
}
