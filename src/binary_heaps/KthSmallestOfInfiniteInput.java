import zain.binary_heap.MaxHeap;
import java.util.ArrayList;
import java.util.List;

public class KthSmallestOfInfiniteInput{

	private	int[] KSmallestElems;
	private int input_len = 0,k=0;
	private List<Integer> inputSoFar = new ArrayList<>();

	public int smallestIs(){return KSmallestElems[0];}

	public KthSmallestOfInfiniteInput(int value_of_k){
		k = value_of_k;
		KSmallestElems = new int[k];
	}
	
	public void addNext(int elem){
		input_len++;
		inputSoFar.add(elem);
		if(input_len == k){
			for(int i=0; i<k; i++) KSmallestElems[i] = inputSoFar.get(i);
			MaxHeap.build(KSmallestElems); 
		}
		if(input_len>k){
			if(elem < KSmallestElems[0]){
				KSmallestElems[0] = elem;
				MaxHeap.bubbleDown(KSmallestElems,0);
			}
		}
		//System.out.println("input so far: ");
		//for (int i=0;i<inputSoFar.size();i++)System.out.print(inputSoFar.get(i)+", ");
		//System.out.println("");
	}

	public static void main(String[] args){
		KthSmallestOfInfiniteInput thirdSmallest = new KthSmallestOfInfiniteInput(3);
		thirdSmallest.addNext(7);
		thirdSmallest.addNext(6);
		thirdSmallest.addNext(5);
		thirdSmallest.addNext(4);
		thirdSmallest.addNext(3);
		thirdSmallest.addNext(2);
		//thirdSmallest.addNext(1);
		System.out.println("So far smallest is: " + thirdSmallest.smallestIs());
	}
}
