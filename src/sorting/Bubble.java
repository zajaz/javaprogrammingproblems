public class Bubble{
	public void sort(int[] a){
		for(int i=0;i<a.length-1;i++){
			for(int j=i+1;j<a.length;j++){
				if(a[i] > a[j]){
					int tmp = a[i];
					a[i] = a[j];
					a[j] = tmp;
				}
			}
		}
	}

	public static void main(String[] args){
		int[] a = {3,4,2,1};
		for(int i=0;i<a.length;i++) System.out.print(a[i] + " ");
		System.out.println("Sorted:");
		Bubble algo = new Bubble();
		algo.sort(a);
		for(int i=0;i<a.length;i++) System.out.print(a[i] + " ");

	}
}
