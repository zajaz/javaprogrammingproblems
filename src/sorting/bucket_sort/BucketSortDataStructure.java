import zain.linkedlist.singlelinkedlist.*;

public class BucketSortDataStructure{
	public SingleLinkedList[] data = new SingleLinkedList[150];
	SLLNode curNode;
	int curIndex;
	public Person getNext(){
		if(curNode == null) increment();
		Person rv = curNode.data;
		increment();
		return rv;
	}
	private void increment(){
		curNode = curNode.next;
		if(curNode == null){
			curIndex++;
			curNode = data[curIndex].get_head();
		}
	}
}
