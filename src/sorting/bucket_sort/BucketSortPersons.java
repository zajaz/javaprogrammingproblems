package bucket_sort;
import zain.linkedlist.singlelinkedlist.*;

class Person{
	public int age;
	public String name;
	public Person(String name){this.name = name;}
}

class BucketSortDataStructure{
	public SingleLinkedList[] data = new SingleLinkedList[150];
	public SLLNode curNode;
	private int curIndex;

	public BucketSortDataStructure(){
		for(int i=0;i<150;i++) data[i] = new SingleLinkedList();
	}	
	
	public Person getNext(){
		while(curNode == null) increment();		
		Person rv = (Person) curNode.data;
		increment();
		return rv;	
	}

	private void increment(){
		if(curNode == null){
			curIndex++;
			curNode = data[curIndex].get_head();
		}
		else curNode = curNode.next;
	}
}

public class BucketSortPersons{
	public void sort(Person[] persons){
		BucketSortDataStructure ds = new BucketSortDataStructure();
		for(int i=0;i<persons.length;i++){
			SingleLinkedList list = ds.data[persons[i].age];
			list.add(new SLLNode<Person>(persons[i]));
		}
		SingleLinkedList list = ds.data[0];
		ds.curNode = ds.data[0].get_head();
		for(int i=0;i<persons.length;i++){
			persons[i] = ds.getNext();
		}
	}

	public static void main(String[] args){
		Person a = new Person("a");
		Person aa = new Person("aa");
		Person b = new Person("b");
		Person bb = new Person("bb");
		Person c = new Person("c");
		Person d = new Person("d");
		a.age = aa.age = 1;
		b.age = bb.age = 2;
		c.age =34;
		d.age =45;
		Person[] persons = new Person[6];
		persons[0]=d;
		persons[1]=c;
		persons[2]=b;
		persons[3]=bb;
		persons[4]=aa;
		persons[5]=a;
		for(int i=0;i<persons.length;i++) System.out.print(persons[i].name+ "  ");
		System.out.println("\nSorted:");
		BucketSortPersons algo = new BucketSortPersons();
		algo.sort(persons);
		for(int i=0;i<persons.length;i++) if(persons[i]!=null) System.out.print(persons[i].name+ "  ");
		System.out.println("");	
	}
}
