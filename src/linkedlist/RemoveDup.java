import zain.linkedlist.singlelinkedlist.*;
import java.util.HashSet;

public class RemoveDup{
	
	public static void run(SLLNode<Integer> head){
		HashSet<Integer> seenValues = new HashSet<>();
		SLLNode<Integer> prev=head, cur=head.next;
		seenValues.add(head.data);
		while(cur != null){
			if(seenValues.contains(cur.data)){
				prev.next = cur.next;
			}
			else{
				 seenValues.add(cur.data);
			     prev = prev.next;
				}
			cur = cur.next;
		}
	}
	
	public static void main(String[] args){
		SingleLinkedList list = new SingleLinkedList();
		for(int i=0;i<10;i++) {
			list.add(new SLLNode<Integer>(1));
			//list.add(new SLLNode<Integer>(i));
		}
		System.out.println("Before");
		list.print();
		run(list.get_head());
		System.out.println("After");
		list.print();

	}
}
