import zain.linkedlist.singlelinkedlist.*;

public class AddLists{
	public static SingleLinkedList add(SLLNode head1, SLLNode head2){
		SLLNode a=head1, b=head2;
		int sum=0,carry=0;
		SingleLinkedList result = new SingleLinkedList();
		while((a!=null) && (b!=null)){
			sum = (int)a.data + (int)b.data + carry;
			carry = sum /10;
			result.add(new SLLNode<Integer>(sum%10));
			a = a.next;
			b=b.next;
		}
		SLLNode larger = (a!=null) ? a : b;
		while(larger!=null){
			sum = (int)larger.data + carry;
			carry = sum/10;
			result.add(new SLLNode<Integer>(sum%10));
			larger = larger.next;
		}
		if(carry > 0) result.add(new SLLNode<Integer>(1));
		return result;
	}
	
	public static void main(String[] args){
		SingleLinkedList f = new SingleLinkedList();
		for(int i=0;i<10;i++) f.add(new SLLNode<Integer>(i));		
		SingleLinkedList s = new SingleLinkedList();
		for(int i=0;i<10;i++) s.add(new SLLNode<Integer>(i));
		SLLNode<Integer> fh, sh;
		fh = f.get_head();
		sh = s.get_head();
		while(fh!=null) {System.out.print(fh.data +" ");fh=fh.next;}
		System.out.println("");
		while(sh!=null) {System.out.print(sh.data +" ");sh=sh.next;}
		System.out.println("");
		SLLNode r= add(f.get_head(),s.get_head()).get_head();
		while(r!=null) {System.out.print(r.data +" ");r=r.next;}
		System.out.println("");
	}
}
