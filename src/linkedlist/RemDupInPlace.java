import zain.linkedlist.singlelinkedlist.*;
import java.util.HashSet;

public class RemDupInPlace{
	public static void remove(SLLNode<Integer> head){
		SLLNode<Integer> cmp=head, prev=head,cur=head.next;
		while(cmp.next.next!=null){
			while(cur!=null){
				if(cur.data == cmp.data) prev.next = cur.next;
				else					 prev = cur;
				cur = cur.next;
			}
			cmp = cmp.next;
			//special case for all duplicates can't look into cmp.next.next outter most loop
			if(cmp == null) return;
			prev = cmp;
			cur=cmp.next;
		}
		if(cmp.data == cur.data) prev.next = cur.next;
	}

	public static void main(String[] args){
		SingleLinkedList list = new SingleLinkedList();
		for(int i=0;i<10;i++) {
			list.add(new SLLNode<Integer>(1));
			//list.add(new SLLNode<Integer>(i));
		}
		System.out.println("Before");
		list.print();
		remove(list.get_head());
		System.out.println("After");
		list.print();

	}
}	
