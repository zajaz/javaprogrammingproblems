public class PiledStack{

	int index=-1,capacity=10;
	int[] stack;
	StackPiler stackPiler;

	PiledStack(StackPiler stackPiler){
		this.stackPiler = stackPiler;
	}
	
	public void push(int val){
		if(index==-1) {
			stack = stackPiler.getNew(capacity);
			index++;
			stack[index]=val;
			index++;
		}
		else if(index == capacity){
			index=0;
			stack = stackPiler.getNew(capacity);
			stack[index]=val;
			index++;
		}
		else{
			stack[index]=val;
			index++;
		}		
	}
	
	public int pop(){
		if(index<0) throw new IllegalStateException("Stack is empty.");
		else if(index == 0){
			stack = stackPiler.getPrev();
			int val = stack[capacity -1];
			index = capacity-2;
			return val;		
		}
		else{
			int val = stack[index -1];
			index--;
			return val;
		}
	}
	
	public static void main(String[] args){
		PiledStack test = new PiledStack(new StackPiler());
		for(int i=0;i<110;i++)	test.push(i);
		for(int i=0;i<20;i++)	System.out.println(test.pop());
	}
}
