import java.util.ArrayList;
public class StackPiler{
	List<int[]> pile = new ArrayList<>();
	
	public int[] getNew(int capacity){
		int[] stack = new int[capacity];
		pile.add(stack);
		System.out.println("New stack created");
		return stack;
	}
	
	public int[] getPrev(){
		pile.remove(pile.size()-1);
		if(pile.size() == 0) throw new IllegalStateException("Stack is empty.");
		else return pile.get(pile.size()-1); 
	}
}
