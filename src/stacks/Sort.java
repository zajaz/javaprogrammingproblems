import zain.Stack;
import zain.StackNode;

public class Sort{
	public static Stack sort(Stack o){
		StackNode tmp;
		Stack n = new Stack();
		n.push(o.pop());
		while(!o.empty()){
			if(n.peek().data > o.peek().data) n.push(o.pop());
			else{
				tmp = o.pop();
				while(!n.empty() && n.peek().data < tmp.data) o.push(n.pop());
				n.push(tmp);
			}
		}
		return n;
	}
	
	public static void main(String[] args){
		Stack o = new Stack();
		for(int i=7;i>0;i--) o.push(new StackNode(i));
		for(int j=0;j<5;j++) o.push(new StackNode(j));
		o.print();
		System.out.println("Sorted");
		Stack n = sort(o);
		n.print();
	}
}
