package zain;

public class Stack{
	StackNode top;
	
	public void push(StackNode item){
		if(item==null) return;
		//if(top==null) top = item;
		//else{
			item.next=top;
			top = item;
		//}
	}
	
	public StackNode pop(){
		if(top == null) return null;
		StackNode item = top;
		top = top.next;
		return item;
	}
	
	public StackNode peek(){
		return top;
	}

	public void print(){
		StackNode tmp = top;
		while(tmp!=null){
			System.out.println(tmp.data);
			tmp = tmp.next;
		}
	}
	
	public boolean empty(){
		return top == null;
	}

}
