package zain.linkedlist.singlelinkedlist;
public class TwoDLL{
	TwoDLLNode head,tail;
	
	public void add(SingleLinkedList list){
		TwoDLLNode item = new TwoDLLNode(list);
		if(head==null) head=tail=item;
		else{
			tail.next=item;
			tail=tail.next;
		}
	}
	public TwoDLL makeCopies(int copies_to_make){
		TwoDLL newList = new TwoDLL();
		for(TwoDLLNode cur=head;cur!=null;cur=cur.next){
			for(int i=0;i<copies_to_make;i++)
				newList.add(cur.list.getDeepCopy());
		}
		return newList;
	}

	public TwoDLLNode getHead(){return head;}

	public void print(){
		TwoDLLNode cur = head;
		int listCount=0;
		while(cur!=null){
			System.out.println("List: "+(++listCount));
			cur.list.print();
			cur=cur.next;
		}
	}
}
