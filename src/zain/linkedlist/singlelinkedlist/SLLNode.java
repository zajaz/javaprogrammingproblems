package zain.linkedlist.singlelinkedlist;
public class SLLNode<T>{
	public T data;
	public SLLNode next;
	public SLLNode(T value){ data=value; }
}
