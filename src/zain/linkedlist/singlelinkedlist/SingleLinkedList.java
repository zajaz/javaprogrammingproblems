package zain.linkedlist.singlelinkedlist;
public class SingleLinkedList{

	private SLLNode head,tail;
	private int size;
	
	public SingleLinkedList getDeepCopy(){
		SingleLinkedList newList = new SingleLinkedList();
		for(SLLNode cur=head;cur!=null;cur=cur.next){
			newList.add(new SLLNode(cur.data));
		}
		return newList;
	}

	public void insertAt(int index, SLLNode item){
		if(index==0) {
			item.next=head;
			head = item;
		}
		else{
			SLLNode cur;
			for(cur=head;index!=1;index--) cur=cur.next;
			item.next=cur.next;
			cur.next=item;			
		}		
	}
	public String toString(){
		String value="";
		for(SLLNode cur=head;cur!=null;cur=cur.next) value += (char)(cur.data);
		return value+="\n";
	}
	
	public void add(SLLNode new_node) throws NullPointerException{
		if(new_node == null) throw new NullPointerException("Can't add a null node");
		size++;
		if(head==null) head = tail = new_node;
		else{
			tail.next = new_node;
			tail = new_node;
		}		
	}

	public SLLNode get_head(){return head;}
	public int length(){return this.size;}

	public SLLNode get_next() throws IllegalStateException{
		if(head == null) throw new IllegalStateException("List was empty.");
		else{
			size--;
			SLLNode temp = head;
			head = head.next;
			return temp;
		}
	}

	public boolean delete(SLLNode node){
		if(head==null) return false;

		if(head.data == node.data){
			head = head.next;
			return true;
		}

		SLLNode cur = head;
		while(cur!=null){
			if(cur.next.data == node.data){
				cur.next = cur.next.next;
				size--;
				return true;
			}
		}
		return false;
	}

	public void print(){
		SLLNode cur = head;
		while(cur != null){
			System.out.println(cur.data);
			cur = cur.next;
		}
	}
	
}
