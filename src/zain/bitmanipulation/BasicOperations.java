package zain.bitmanipulation;

public class BasicOperations{
	public static int bitAt(int num, int index){
		int mask = 1 << index;
		int bit = (num & mask) >> index;
		return bit;
	}
	
	public static int invertBitAt(int num, int index){
		//Theory: bit xor 0 is bit itself, bit xor 1 inverts it.
		return num ^ (1<<index);
	}

	public static void iterateBitsUpto(int n, int index){
		for(int i=0; i<=index; i++){
			System.out.println("bit at "+i+" is "+BasicOperations.bitAt(n,i));
		}
	}

	public static int countSetBits(int n){
		int count = 0;
		while(n != 0){
			n = n >>> 1; // signed shift on a neg no never terminates.
			if((n & 1) ==1) count++; //remove if to get position of last set bit.
		}
		return count;
	}

	public static void main(String[] args){
		// Interesting opservations:
		// System.out.println(-1==~0);
		int a = Integer.parseInt("01100",2);
		//int a =-7;
		System.out.println("Orignal: "+ Integer.toBinaryString(a));
		int index = 0;
		//System.out.println("Bit at " + index + " is "+BasicOperations.bitAt(a, index));
		//System.out.println("New    : "+ Integer.toBinaryString(BasicOperations.invertBitAt(a,index)));
		//BasicOperations.iterateBitsUpto(a, 3);
		System.out.println("Number of set bits: "+ BasicOperations.countSetBits(a));
	}
}
