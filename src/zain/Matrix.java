package zain;
public class Matrix{
	private static void traverse(int[][] A, String action){
		int count=0;
		for(int i=0;i<A.length;i++){
			for(int j=0;j<A[0].length;j++){
				if(action == "init") A[i][j] = ++count;
				else if(action =="print") System.out.print(A[i][j] + "	 "); 
			}
			if(action == "print")System.out.println("");
		}
	}
	
	public static void print(int[][] matrix){
		traverse(matrix, "print");
	}

	public static void init(int[][] matrix){
		traverse(matrix, "init");
	}
}
