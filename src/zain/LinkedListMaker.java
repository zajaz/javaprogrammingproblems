package zain;
import java.util.LinkedList;
import java.util.HashMap;

public class LinkedListMaker{
	HashMap<Integer, LinkedList> linkedLists = new HashMap<>();
	
	public void add(int height, BstNode item){
		if(linkedLists.containsKey(height)){
			linkedLists.get(height).add(item);
		}
		else{
			LinkedList<BstNode> list = new LinkedList<>();
			list.add(item);
			linkedLists.put(height, list);
		}
	}
	public int size(){return linkedLists.size();}
	public LinkedList<BstNode> get(int height){return linkedLists.get(height);}
}
