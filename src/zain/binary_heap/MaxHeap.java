package zain.binary_heap;

public class MaxHeap{
	public static void build(int[]A){
		for(int i = (int) Math.floor(A.length/2);i>=0;i--)
			MaxHeap.bubbleDown(A,i);
		
		//for(int i=0;i<A.length;i++)System.out.print(A[i]+", ");
	}
	
	public static void bubbleDown(int[] A, int root){
		int max = root;
		int left = root*2+1;
		int right = root*2+2;
		if(left< A.length && A[left]>A[max])    max=left;
		if(right<A.length && A[right] > A[max]) max=right;
		if(max != root){
			int tmp = A[root];
			A[root] = A[max];
			A[max]  = tmp;
			bubbleDown(A,max);
		}
	}
	
	public static void bubbleUp(int[] A, int root){
		int parent = (int) Math.floor(root-1/2);
		if(A[parent] < A[root]){
			int tmp = A[root];
			A[root] = A[parent];
			A[parent]  = tmp;
			bubbleUp(A,parent);
		}
	}
	
	public static void main(String[] args){
		int A[] = {1,2,3,4,5};
		MaxHeap.build(A);
		System.out.println("Post build:");
		for(int i=0;i<A.length;i++)System.out.print(A[i]+", ");
		System.out.println("");
	}
}
