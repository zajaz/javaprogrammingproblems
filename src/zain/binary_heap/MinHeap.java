package zain.binary_heap;

public class MinHeap{
	public static void build(int[] A){
		for(int i=(int)Math.floor(A.length/2);i>=0;i--)
			MinHeap.bubbleDown(A, i);
		for(int i=0;i<A.length;i++) System.out.println(A[i]);
	}

	public static void bubbleDown(int[] A, int root){
		int min = root;
		int left = root*2+1;
		int right = root*2+2;
		if(left < A.length && A[left] < A[min])   min = left;
		if(right < A.length && A[right] < A[min]) min = right;
		if(root != min)	{
			int temp = A[root];
			A[root] = A[min];
			A[min] = temp;
			bubbleDown(A,min);
		}
	}
	
	public static void bubbleUp(int[] A, int root){
		int parent = (int)Math.floor((root-1)/2);
		if(A[root] > A[parent]){
			int temp = A[root];
			A[root] = A[parent];
			A[parent] = temp;
			bubbleUp(A, parent);
		}
	}
}
