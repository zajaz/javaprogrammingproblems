package zain;
public class BstWithParent{
	BstNodeP root;
	
	public void insert(BstNodeP item){root = insert(root,item);}

	public BstNodeP insert(BstNodeP root, BstNodeP item){
		if(root==null){
			root = item;
			return root;
		}
		else if(item.data < root.data){
			root.left = insert(root.left, item);
			root.left.parent = root;
		}
		else{
			root.right = insert(root.right, item);
			root.right.parent = root;
		}
		return root;
	}
	
	public void print(){print(root);}	
	public BstNodeP getRoot(){return root;}

	private void print(BstNodeP root){
		if(root==null)return;
		print(root.left);
		System.out.println(root.data);
		print(root.right);
	}
	
	public static void main(String[] args){
		BstWithParent tree = new BstWithParent();
		for(int i=0;i<3;i++) tree.insert(new BstNodeP(i));
		//tree.print();
	}
}
