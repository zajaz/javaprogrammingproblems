package zain;

public class Bst{
	private BstNode root;
	
	public void insert(BstNode item){
		root = insert(root, item);
	}
	
	// read as: insert in root this item is:
	// insert in roots left this item or 
	// insert in roots right this item
	private BstNode insert(BstNode root, BstNode item){
		if(root == null) return item;		
		else if(root.data > item.data)	root.left = insert(root.left, item);			
		else root.right = insert(root.right, item);
		return root;
	}	
	
	public void print(){ print(root);}

	public BstNode get_root(){return root;}

	public void print(BstNode root){
		if(root == null) return;
		print(root.left);
		System.out.println(root.data);
		print(root.right);
	}
	
	/*public static void main(String[] args){
	Bst tree = new Bst();
		for(int i=0;i<10;i++) tree.insert(new BstNode(i));
		System.out.println("should print 9 values");
		tree.print();
	}*/
}
