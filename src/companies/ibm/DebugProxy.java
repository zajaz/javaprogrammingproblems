import java.lang.reflect.*;

interface Ic1{
	public void method1(int arg1);
	public void method2(int arg);
}

class c1 implements Ic1{
	public void method1(int arg1){
		System.out.println("Inside method1 of c1");
	}
	public void method2(int arg){
		System.out.println("Inside method2 of c1");
	}
}

class DynamicDebugProxy implements InvocationHandler {
    private Object obj;
	private DynamicDebugProxy(Object obj) {  this.obj = obj;}
	
	public static Object newInstance(Object obj) {
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj
                .getClass().getInterfaces(), new DynamicDebugProxy(obj));
    }
    

    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
        Object result;
        try {
			String msg = "Entering "+ m.getName() + " with args: ";
			for(Object arg:args) msg += " "+arg.toString();
            System.out.println(msg);
            long start = System.nanoTime();
            result = m.invoke(obj, args);
            long end = System.nanoTime();
            //System.out.println(String.format("%s took %d ns", m.getName(), (end-start)) );
        } 
		catch (InvocationTargetException e) {
            throw e.getTargetException(); 
        } 
		catch (Exception e) {
            throw new RuntimeException("unexpected invocation exception: " + e.getMessage());
        } 
		finally {
            System.out.println("Leaving method " + m.getName() +"\n");
        }
        return result;
    }
}

public class DebugProxy{
	public static void main(String[] args){
		Ic1 objUnderProxy = (Ic1) DynamicDebugProxy.newInstance(new c1());
		objUnderProxy.method1(1);
		objUnderProxy.method2(1);
	}
}
